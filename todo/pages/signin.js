import SignInForm from "@/components/Template/SignInForm";

const signin = () => {
  return (
    <>
      <SignInForm />
    </>
  );
};

export default signin;
