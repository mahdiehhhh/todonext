import UserTodos from "@/models/userTodo";
import connectDB from "@/utils/connectDB";
import { sortedTods } from "@/utils/sortedTodos";
import { getSession } from "next-auth/react";

async function handler(req, res) {
  try {
    await connectDB();
  } catch (error) {
    return res
      .status(500)
      .json({ status: "failed", message: "you are not connect" });
  }

  const session = await getSession({ req });

  console.log(session, "session");
  if (!session) {
    return res
      .status(401)
      .json({ status: "failed", message: "You are not logged in!" });
  }

  const userTodo = await UserTodos.findOne({ email: session.user.email });

  if (!userTodo) {
    return res
      .status(404)
      .json({ status: "failed", message: "user dosent exist" });
  }

  if (req.method === "POST") {
    const { title, status } = req.body;

    if (!title || !status) {
      return res
        .status(422)
        .json({ status: "failed", message: "invalid Data" });
    }

    userTodo.todos.push({ title, status });

    userTodo.save();

    res.status(201).json({ status: "success", message: "Todo create" });
  } else if (req.method === "GET") {
    const sortedTodo = sortedTods(userTodo.todos);

    res.status(200).json({ status: "success", data: sortedTodo });
  }
}
export default handler;
