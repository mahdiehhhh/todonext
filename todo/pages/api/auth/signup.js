import UserTodos from "@/models/userTodo";
import { hashPassword } from "@/utils/auth";
import connectDB from "@/utils/connectDB";

async function handler(req, res) {
  if (req.method !== "POST") return;
  try {
    await connectDB();
  } catch (error) {
    res
      .status(500)
      .json({ status: "failed", message: "You are not connect to DB" });
  }

  const { email, password } = req.body;

  if (!email || !password) {
    return res.status(422).json({ status: "failed", message: "invalid Data" });
  }

  const existingUser = await UserTodos.findOne({ email: email });

  if (existingUser) {
    return res.status(422).json({ status: "failed", message: "user Existing" });
  }

  const hashedPassword = await hashPassword(password);

  const newTodoUser = await UserTodos.create({
    email: email,
    password: hashedPassword,
  });

  console.log(newTodoUser);

  res.status(201).json({ status: "success", message: "user create" });
}
export default handler;
