import { useState } from "react";
import RadioButton from "@/components/element/RadioButton";
import { GrAddCircle } from "react-icons/gr";
import { BsAlignStart } from "react-icons/bs";
import { FiSettings } from "react-icons/fi";
import { AiOutlineFileSearch } from "react-icons/ai";
import { MdDoneAll } from "react-icons/md";
import axios from "axios";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
const AddTodo = () => {
  const [title, setTitle] = useState("");
  const [status, setStatus] = useState("todo");
  const addHandler = async () => {
    await axios
      .post("/api/todos", { title, status })
      .then((res) => {
        if (res.data.status == "success") {
          toast.success("Todo Added", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  return (
    <div className='add--form'>
      <h2>
        <GrAddCircle />
        Add New Todo
      </h2>

      <div className='add-form__input'>
        <div className='add-form__input--first'>
          <label htmlFor='title'>Title:</label>
          <input
            id='title'
            type='text'
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          />
        </div>
        <div className='add-form__input--second'>
          <RadioButton
            value='todo'
            status={status}
            setStatus={setStatus}
            title='Todo'>
            <BsAlignStart />
          </RadioButton>

          <RadioButton
            status={status}
            setStatus={setStatus}
            value='inProgress'
            title='In Progress'>
            <FiSettings />
          </RadioButton>

          <RadioButton
            status={status}
            setStatus={setStatus}
            value='review'
            title='Review'>
            <AiOutlineFileSearch />
          </RadioButton>

          <RadioButton
            status={status}
            setStatus={setStatus}
            value='done'
            title='Done'>
            <MdDoneAll />
          </RadioButton>
        </div>
        <button onClick={addHandler}>Add</button>
        <ToastContainer />
      </div>
    </div>
  );
};

export default AddTodo;
