import SignUpForm from "@/components/Template/SignUpForm";

const signup = () => {
  return (
    <>
      <SignUpForm />
    </>
  );
};

export default signup;
