const { Schema, models, model } = require("mongoose");

const userTodoSchema = new Schema({
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  name: String,
  lastName: String,
  todos: [{ title: String, status: String }],
  createdAt: {
    type: String,
    default: () => Date.now(),
    immutable: true,
  },
});

const UserTodos = models.UserTodos || model("UserTodos", userTodoSchema);

export default UserTodos;
