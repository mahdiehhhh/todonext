import Link from "next/link";
import { useRouter } from "next/router";
import { signIn, useSession } from "next-auth/react";
import { useEffect, useState } from "react";
const SignInForm = () => {
  const [formData, setFormData] = useState({ email: "", password: "" });

  const { status } = useSession();
  const router = useRouter();

  useEffect(() => {
    if (status == "authenticated") {
      router.replace("/");
    }
  }, [status]);

  const inputHandler = (e) => {
    const value = e.target.value;
    setFormData({ ...formData, [e.target.name]: value });
  };

  const loginHandler = async () => {
    const res = await signIn("credentials", {
      email: formData.email,
      password: formData.password,
      redirect: false,
    });
    if (!res.error) {
      router.push("/");
    }
  };
  return (
    <div className='signin-form'>
      <h3>Login Form</h3>

      <input
        type='text'
        value={formData.email}
        name='email'
        placeholder='Email'
        onChange={(e) => inputHandler(e)}
      />

      <input
        type='password'
        value={formData.password}
        placeholder='Password'
        name='password'
        onChange={(e) => inputHandler(e)}
      />

      <button onClick={loginHandler}>Login</button>

      <div>
        <p>create an account?</p>
        <Link href='/signup'>Sign Up</Link>
      </div>
    </div>
  );
};

export default SignInForm;
