import axios from "axios";
import { useSession } from "next-auth/react";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

const SignUpForm = () => {
  const [formData, setFormData] = useState({ email: "", password: "" });

  const { status } = useSession();
  const router = useRouter();

  useEffect(() => {
    if (status == "authenticated") {
      router.replace("/");
    }
  }, [status]);

  const inputHandler = (e) => {
    const value = e.target.value;
    setFormData({ ...formData, [e.target.name]: value });
  };

  const registerHandler = async () => {
    axios
      .post("/api/auth/signup", {
        email: formData.email,
        password: formData.password,
      })
      .then((res) => {
        if (res.status == 201) {
          router.push("/signin");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div className='signin-form'>
      <h3>Registration Form</h3>

      <input
        type='text'
        value={formData.email}
        name='email'
        placeholder='Email'
        onChange={(e) => inputHandler(e)}
      />

      <input
        type='password'
        value={formData.password}
        placeholder='Password'
        name='password'
        onChange={(e) => inputHandler(e)}
      />

      <button onClick={registerHandler}>Register</button>

      <div>
        <p>Have an account?</p>
        <Link href='/signin'>Sign in</Link>
      </div>
    </div>
  );
};

export default SignUpForm;
