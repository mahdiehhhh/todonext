import axios from "axios";
import { useEffect, useState } from "react";

const HomePage = () => {
  const [todos, setTodos] = useState([]);
  useEffect(() => {
    allTodos();
  }, []);

  const allTodos = async () => {
    await axios
      .get("/api/todos")
      .then((res) => {
        setTodos(res.data.data);
        // console.log(res.data.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div className='home-page'>
      <div className='home-page--todo'>
        <p>Todo</p>
      </div>

      <div className='home-page--inProgress'>
        <p>inProgress</p>
      </div>

      <div className='home-page--done'>
        <p>Done</p>
      </div>
      <div className='home-page--todo'>
        <p>Todo</p>
      </div>
    </div>
  );
};

export default HomePage;
